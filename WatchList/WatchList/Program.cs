﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WatchList.Data;
using WatchList.Utils;

namespace WatchList
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {                   
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var UM = services.GetRequiredService<UserManager<IdentityUser>>();

                    //Verificar dados iniciais
                    new SeedData(UM).SemearDadosIniciaisAsync();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }

            }

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                          .UseStartup<Startup>()
                          .ConfigureKestrel((context, options) =>
                          {
                              if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                              {
                                  // bind to socket
                                  if (args.Contains("dev"))
                                  {
                                      options.ListenUnixSocket("/tmp/kestrel-dev.watchlist.sock");
                                  }
                                  else
                                  {
                                      options.ListenUnixSocket("/tmp/kestrel-watchlist.sock");
                                  }
                              }
                              else
                              {
                                  options.Listen(IPAddress.Loopback, 6565);
                              }

                          })
                          .ConfigureLogging((hostingContext, logging) =>
                          {
                              logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                              logging.AddConsole();
                          });
        }
    }
}
