﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WatchList.Utils
{
    public class AppSettings
    {
        private static readonly RandomNumberGenerator _rng;

        //Especificar o ambiente
        public static IConfiguration Configuration
        {
            get; set;
        }

        //Connection string para a BD
        public static string BuscarConnStr()
        {
            return Configuration.GetConnectionString("DefaultConnection");
        }        
    }
}
