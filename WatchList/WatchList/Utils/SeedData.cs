﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WatchList.Utils
{
    public class SeedData
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;

        public SeedData(UserManager<IdentityUser> gestorU)
        {
            _userManager = gestorU;
        }

        public async void SemearDadosIniciaisAsync()
        {
            if (!_userManager.Users.Any())
            {
                var novoU = new IdentityUser
                {
                    NormalizedUserName = "Admin",
                    UserName = "Admin",
                    Email = "admin@watchlist.pt"
                };

                var resultado = await _userManager.CreateAsync(novoU, "admin");

                if (resultado.Succeeded)
                    Debug.Write("Admin was resgitered with success!");
                else
                    Debug.Write("Erro registering the Admin!");
            }
            else
                Debug.Write("Admin already resgitered!");

        }
    }
}
